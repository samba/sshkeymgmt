-- MySQL dump 10.11
--
-- Host: localhost    Database: ssh_key_register
-- ------------------------------------------------------
-- Server version	5.0.70-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorization`
--

DROP TABLE IF EXISTS `authorization`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `authorization` (
  `email` varchar(255) NOT NULL,
  `insertion` timestamp NOT NULL default '0000-00-00 00:00:00',
  `hostname` varchar(255) NOT NULL,
  `username` varchar(64) NOT NULL,
  `activation` timestamp NOT NULL default '0000-00-00 00:00:00',
  `expiration` timestamp NOT NULL default '0000-00-00 00:00:00',
  `hosthash` varchar(255) NOT NULL,
  PRIMARY KEY  (`email`,`insertion`,`hostname`,`username`),
  KEY `fk_auth_host` (`hostname`,`email`),
  CONSTRAINT `fk_auth_host` FOREIGN KEY (`hostname`, `email`) REFERENCES `hosts` (`hostname`, `email`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_auth_key` FOREIGN KEY (`email`, `insertion`) REFERENCES `keys` (`email`, `insertion`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 0 kB; (`email` `insertion`) REFER `ssh_key_regi';
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `authorized_keys`
--

DROP TABLE IF EXISTS `authorized_keys`;
/*!50001 DROP VIEW IF EXISTS `authorized_keys`*/;
/*!50001 CREATE TABLE `authorized_keys` (
  `hostname` varchar(255),
  `email` varchar(255),
  `username` varchar(64),
  `activation` timestamp,
  `expiration` timestamp,
  `insertion` timestamp,
  `pubkey` text,
  `hosthash` varchar(255),
  `description` varchar(255),
  `enabled` varchar(1)
) ENGINE=MyISAM */;

--
-- Table structure for table `hosts`
--

DROP TABLE IF EXISTS `hosts`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `hosts` (
  `hostname` varchar(255) NOT NULL default '',
  `description` varchar(255) default NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY  USING BTREE (`hostname`,`email`),
  KEY `fk_host_owner` (`email`),
  CONSTRAINT `fk_host_owner` FOREIGN KEY (`email`) REFERENCES `users` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `keys`
--

DROP TABLE IF EXISTS `keys`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `keys` (
  `email` varchar(255) NOT NULL,
  `pubkey` text NOT NULL,
  `insertion` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `enabled` varchar(1) NOT NULL default 'n',
  PRIMARY KEY  (`email`,`insertion`),
  CONSTRAINT `fk_key_owner` FOREIGN KEY (`email`) REFERENCES `users` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `session_log`
--

DROP TABLE IF EXISTS `session_log`;
/*!50001 DROP VIEW IF EXISTS `session_log`*/;
/*!50001 CREATE TABLE `session_log` (
  `sessionid` varchar(32),
  `email` varchar(255),
  `ipaddr` varchar(15),
  `start` timestamp,
  `end` timestamp,
  `current` int(1)
) ENGINE=MyISAM */;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sessions` (
  `sessionid` varchar(32) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ipaddr` varchar(15) NOT NULL,
  `start` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `end` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`sessionid`),
  KEY `fk_session_ownership` (`email`),
  CONSTRAINT `fk_session_ownership` FOREIGN KEY (`email`) REFERENCES `users` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `users` (
  `email` varchar(255) NOT NULL,
  `password` varchar(128) NOT NULL,
  `viewskeys` varchar(1) NOT NULL default 'n',
  `sessionid` varchar(32) default NULL,
  PRIMARY KEY  (`email`),
  KEY `fk_user_current_session` (`sessionid`),
  CONSTRAINT `fk_user_current_session` FOREIGN KEY (`sessionid`) REFERENCES `sessions` (`sessionid`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `authorized_keys`
--

/*!50001 DROP TABLE `authorized_keys`*/;
/*!50001 DROP VIEW IF EXISTS `authorized_keys`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `authorized_keys` AS select `h`.`hostname` AS `hostname`,`u`.`email` AS `email`,`a`.`username` AS `username`,`a`.`activation` AS `activation`,`a`.`expiration` AS `expiration`,`k`.`insertion` AS `insertion`,`k`.`pubkey` AS `pubkey`,`a`.`hosthash` AS `hosthash`,`h`.`description` AS `description`,`k`.`enabled` AS `enabled` from (((`authorization` `a` join `keys` `k` on((`k`.`email` = `a`.`email`))) join `hosts` `h` on((`h`.`email` = `a`.`email`))) join `users` `u` on((`u`.`email` = `a`.`email`))) where ((`u`.`email` = `a`.`email`) and (`a`.`hostname` = `h`.`hostname`) and (`k`.`email` = `a`.`email`) and (`k`.`insertion` = `a`.`insertion`) and (`a`.`activation` < now()) and (`a`.`activation` <> 0) and (`a`.`expiration` > now()) and (`a`.`expiration` <> 0) and (`k`.`enabled` = _utf8'y')) */;

--
-- Final view structure for view `session_log`
--

/*!50001 DROP TABLE `session_log`*/;
/*!50001 DROP VIEW IF EXISTS `session_log`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `session_log` AS select `s`.`sessionid` AS `sessionid`,`u`.`email` AS `email`,`s`.`ipaddr` AS `ipaddr`,`s`.`start` AS `start`,`s`.`end` AS `end`,((`s`.`sessionid` = `u`.`sessionid`) and (`s`.`end` > now())) AS `current` from (`sessions` `s` join `users` `u` on((`u`.`email` = `s`.`email`))) */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-06-20  0:20:43
