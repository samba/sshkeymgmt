<?php

require('core/request.inc.php');
$m = require('core/database.inc.php');
require('core/auth.inc.php');

$email = request('email', '%.255s');
$password = request('password', 'SALT&%.255s');

session_start();
if(valid_user($m, $email, $password)){
	login_session($m, $email);
	header('Location: panel.php?' . SID);
}else{
	logout_session($m, $email);
	header('Location: index.html?badlogin=true');
}

?>
