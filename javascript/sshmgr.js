if(!jQuery){
	throw 'Eh! I need jQuery!';
}

/* TODO... requests to panel.php, etc
 * list keys
 * list hosts
 * selecting a key (and no host) allows key revision
 * selecting a host (and no key) allows host revision
 * selecting a key and a host allows auth revision/instantiation, etc
 * 
 *
 */

function debug(){
	if(console)
		console.debug(debug.arguments)
}


SSHManager = {
	remote: null,
	onload: function (){		
		l = window.location;
		this.remote = l.protocol + '//' + l.host + l.pathname;
		jQuery.getJSON(this.remote, { action: 'list', object: 'tasks' }, SSHManager.task.load);
		
		// SSHManager.keys.load();
		// SSHManager.hosts.load();

	},

	task: {
		load: function(j){
			if(j.length > 30)
				return false;
			var link, p = $('div#top > ul.left');
			for(var i in j){
				if(j[i]) {
					li = $('<li></li>');
					a = $('<a href="'+j[i]+'">'+i+'</a>');
					k = function(){
						$(this).removeClass('current');
						SSHManager.iface.deactivate(this);
					};
					a.click(function(){
						$('a', $(this).parent().siblings()).each(k);
						SSHManager.iface.activate(this);
						$(this).addClass('current');
						return false;
					});
					li.append(a);
					p.append(li);
				}
			}
		}
	},
	iface: {
		deactivate: function(i){
			// debug('deactivating', i);
			$(i.hash).hide();
		},
		activate: function(i){
			// debug('activating', i);
			$(i.hash).show();
		}
	},

	keys: {},
	hosts: {}

};

// Register my onload function...
$(SSHManager.onload);

