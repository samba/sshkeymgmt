<?php
header('Content-type: text/plain');

require('core/request.inc.php');
$m = require('core/database.inc.php');

$alphanum='/[^a-zA-Z0-9_.-]+/';
$host = preg_replace($alphanum, '', request('host', '%.255s'));
$user = preg_replace($alphanum, '', request('user', '%.128s'));
$hash = preg_replace($alphanum, '', request('hash'));

$q = 'SELECT email,pubkey,activation,`expiration` FROM authorized_keys WHERE `username`="%s" AND hostname="%s" AND hosthash="%s"';
$q = sprintf($q, $user, $host, $hash);



$r = $m->query($q);
echo '# SSH key manager service {' . "\n";
if(!$r) echo '# an error occurred.';
while($row = $r->fetch_assoc()){
	printf("# email: %s; activated: %s; expires: %s;\n%s\n", 
		$row['email'], $row['activation'], $row['expiration'], $row['pubkey']
	);
}
echo '# } end SSH key manager service';

?>
