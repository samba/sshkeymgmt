<?php

require('core/request.inc.php');
$m = require('core/database.inc.php');
require('core/auth.inc.php');


header('Content-type: text/plain');
session_start();

$user = request('user', '%.255s');
$passwd = request('pass', 'SALT&%.255s');

if(valid_session($m, $user) or valid_user($m, $user, $passwd)){
	if(count($_FILES) < 1){
		echo 'Warning: no files were uploaded!';
	}
	foreach(array_keys($_FILES) as $k){
		$key = file_get_contents($_FILES[$k]['tmp_name']);
		$r = insert_key($m, $user, $passwd, $key);
		if($r) echo 'Key insertion successful: ' . $k . "\n";
		else echo 'Key insertion failed: ' . $k . ' ' . mysqli_err($m) . "\n";
		unlink($_FILES[$k]['tmp_name']);
	}
}else{
	echo 'Not Authorized.' . "\n";
}



function insert_key(& $m, $u, $p, & $k){
	$s = $m->prepare('INSERT INTO `keys` (email, pubkey, insertion) VALUES (?, ?, ?)');
	$s->bind_param('sss', $u, $k, date ("Y-m-d H:i:s", time()) );
	$r = $s->execute();
	$s->close();
	return $r;
}
 


?>
