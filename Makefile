DBNAME=ssh_key_register
DBUSER=root
DBHOST=localhost


.PHONY: install setup.sql

install:
# TODO: execute the SQL, configure, etc


revisions:
	$(shell echo $$EDITOR) $(shell find ./ -type f | grep -v .git )

setup.sql:
	mysqldump -u $(DBUSER) -h $(DBHOST) -p $(DBNAME) -d > $@

sanitary: setup.sql
	
