<?php

require('core/request.inc.php');
$m = require('core/database.inc.php');
require('core/auth.inc.php');

session_start();

$a = request('action');
if(! valid_session($m, $_SESSION['email'])){
	logout_session($m, $_SESSION['email']);
	switch($a){
		case null: 
			header('Location: index.php?badsession=true');
			break;
		case 'load':
		case 'save':
		case 'drop':
		default:
			header('Content-type: text/plain');
			print 'Session invalid.';
	}	
	exit(1);
}else{
	switch($a){
		case 'logout':
			logout_session($m, $_SESSION['email']);
			header('Location: index.php');
			break;
		case null:
			readfile('panel.html');
			break;
		case 'list':
			echo json_encode(build_list(request('object')));
			break;
		case 'load':
		case 'save':
		case 'drop':
		default:
			header('Content-type: text/plain');	
			echo 'Uh... what?';
	}
	exit(0);

}

function build_list($o){
	$r = array();
	if(!is_string($o)) return false;
	switch($o){
		case 'tasks':
			$r = array(
				'Control Panel' => '#main',
				'Upload Key' => '#key_upload',
				'Register Host' => '#host_register'
				);
			
			break;	
		case 'keys':
		case 'hosts':
		default:
			return null;
	}
	// return an array/hash of the object-type list
	return $r;	
}



function action_bar ($a){
	$r = '';
	$nav_actions = array(
	'default' => 'Control Panel',
	'new_key' => 'New Key',
	'new_host' => 'New Host'
	);

	$r = '<ul class="left">';
	foreach(array_keys($nav_actions) as $k){
		if($k == $a){
			$r .= sprintf('<li><h1>%s</h1></li>', $nav_actions[$k]);
		}else {
			$r .= sprintf('<li><a href="?action=%s">%s</a></li>', $k, $nav_actions[$k]);
		}
	}

	$r .= '</ul>';
	return $r;
}



function main_panel (& $m) {
	$keys = '<ul>';
	$l = key_list($m);
	foreach(array_keys($l) as $k){
		$keys .= sprintf('<li><a href="?action=edit_key?insertion=%s">%s</a> (%d authorizations)</li>', 
			$l[$k]['insertion'], $l[$k]['descriptor'], $l[$k]['count']);
	}
	$keys .= '</ul>';

	$hosts = '<ul>';
	$l = host_list($m);
	foreach(array_keys($l) as $h){
		$hosts .= sprintf('<li><a href="?action=edit_host?hostname=%s">%1$s</a> %s</li>', 
			$l[$h]['hostname'], $l[$h]['description']);
	}
	$hosts .= '</ul>';

	$LAYOUT = <<<EOF
EOF;
	return $LAYOUT;
}

function key_list (& $m) {
	$s = $m->prepare('SELECT k.pubkey, k.insertion, k.enabled, c.C as count 
		FROM  `keys` k LEFT OUTER JOIN authorized_key_counts c 
		ON c.email=k.email AND c.insertion=k.insertion WHERE k.email=?');
	$e = $_SESSION['email'];
	$s->bind_param('s', $e);
	$s->execute();
	$s->bind_result($p, $i, $e, $c);
	$r = array();
	while($s->fetch()){
		preg_match('/^(\S+)\s+(\S+)\s+(.*)$/m', $p, $d);
		if(is_array($d) and isset($d[3]))
			$d = $d[3];
		else $d = null;
		$r[] = array(
			'insertion' => $i,
			'enabled' => $e,
			'count' => $c,
			'pubkey' => $p,
			'descriptor' => $d
		);
	}	
	$s->close();
	return $r;
}

function host_list (& $m) {
	$s = $m->prepare('SELECT hostname, description FROM hosts WHERE email=?');
	$e = $_SESSION['email'];
	$s->bind_param('s', $e);
	$s->execute();
	$s->bind_result($h, $d);
	$r = array();
	while($s->fetch()){
		$r[] = array(
			'hostname' => $h,
			'description' => $d
		);
	}	
	$s->close();
	return $r;

}



?>
