<html>

	<head>
		<link rel='stylesheet' href='styles.css'/>
	</head>

	<body>
		<table id='index-main'>
			<tr><td>
					<p>This is a tool to let me upload keys in a secure fashion to a central repository, so I can keep track of them. 
					<p>It's a work in progress. Future versions will allow automated authorization of keys, but as this is a critical point of security, I'm still in planning - not something to be taken lightly.


					<p>To upload a key to this system:
					<code>curl  -k -F user=$USERNAME -F pass=$PASSWORD -F key=@$HOME/.ssh/id_rsa.pub https://<?php echo $_SERVER['SERVER_NAME']; ?>/ssh/upload.php</code>

					<ul>
						<li>Note: some web servers don't handle all HTTP headers correctly 
						(as documented by <a href="http://redmine.lighttpd.net/issues/show/1017">this bug</a>). 
						To work around this, use curl's '-H' option... e.g. <code> -H 'Expect:' </code>
						The use of curl is recommended over wget, since curl supports multipart/form-data encoding internally, which allows file uploads. 
						If you know of a concise way of handling such uploads via wget, please send recommendations to: &lt;support *at* befreely.dyndns.org&gt;.
					</ul>
					</td><td>
					<form name='login' method='post' action='login.php'>
						<fieldset><legend>Log In</legend>
							<label>Email Address <input name='email' type='text'/></label>
							<label>Password <input name='password' type='password'/></label>
							<label><input type='submit'/></label>
						</fieldset>
					</form>
			</td></tr>
		</table>
	</body>


</html>

