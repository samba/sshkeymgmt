<?php

$config = require('core/database.config.inc.php');

function mysqli_err ( &$m ){
	return sprintf('[%d] %s', $m->errno, $m->error);
}



$m = new mysqli($config['host'], $config['user'], $config['password'], $config['database']);
if(!$m){
	user_error(mysqli_err($m));
}

return $m;


?>
