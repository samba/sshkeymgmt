<?php

session_start();

function valid_user(& $m, $u, $p){
	$s = $m->prepare('SELECT COUNT(*) as C FROM users WHERE email=? AND password=PASSWORD(?)');
	$allows_key_viewing='y';
	$s->bind_param('ss', $u, $p);
	$s->execute();
	
	$s->bind_result($c);
	$s->fetch();
	if($c == 1) $r = true;
	else $r = false;

	$s->close();

	return $r;
}

function valid_session (&$m, $u){
	$s = $m->prepare('SELECT COUNT(*) as C FROM session_log WHERE email=? AND current=1 AND sessionid=? AND ipaddr=?');
	$id = session_id();
	$s->bind_param('sss', $u, $id, $_SERVER['REMOTE_ADDR']);
	$s->execute();
	$s->bind_result($c);
	$s->fetch();
	if($c == 1) $r = true;
	else $r = false;
	$s->close();
	return $r;
}


function login_session ( &$m, $u ){
	session_start();
	$_SESSION = array( 'email' => $u );
	$_COOKIE[session_name()] = session_id();
	$id = session_id();
	$start = date('Y-m-d H:i:s', strtotime('now'));
	$end = date('Y-m-d H:i:s', strtotime('now + 30 minutes'));
	$s = $m->prepare('INSERT INTO sessions (sessionid, email, ipaddr,end) VALUES (?,?,?,?) ON DUPLICATE KEY UPDATE start=?, end=?');
	$s->bind_param('ssssss', $id, $u,  $_SERVER['REMOTE_ADDR'], $end, $start, $end);
	$s->execute();
	$s->close();
	$s = $m->prepare('UPDATE users SET sessionid=? WHERE email=?');
	$s->bind_param('ss', $id, $u);
	$s->execute();
	$s->close();
}

function logout_session (&$m, $u){
	session_start();
	$id=session_id();
	$now = date('Y-m-d H:i:s', strtotime('now'));
	$_SESSION = array();
	$s = $m->prepare('UPDATE sessions SET end=? WHERE sessionid=? AND ipaddr=?');
	$s->bind_param('sss', $now, $id, $_SERVER['REMOTE_ADDR']);
	$s->execute();
#	echo mysqli_err($s);
	$s->close();
	session_destroy();
	session_write_close();
}
?>
